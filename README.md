# Magento Translation Helper
This module will help you in finding missing/incomplete translations for your modules  
It tries to identify all files (XML, PHP, phtml, html, CSV) which are related to your module  
and scans them for translatable strings (which means string inside the __() method)

### Prerequisites
You need at least some basic knowledge of how Magento translations work,  
however it is good to have some in depth knowledge of the internals  
(i.e. why it is a really bad idea to define your translation file with an lowercase xml node like `<mycompany_mymodule>`) 

### Settings
(under System -> Configuration -> poebel -> Translation Helper)  

* Locales  
Select the locales you want to check  
* Only from Module Scope  
If this is enabled, translations are only searched in the modules own scope,  
almost like the behaviour of enabled developer mode

### Limitations
* If the translated string is the same as the original string, it will be detected as "missing"  
This is to prevent wrong/missing translations from showing up, but also makes it somehow difficult to "translate" for other english locales
* Detection of translatable strings is far from perfect, due to possible complex usage i.e. `$this->__('string with \'escaped\' stuff %s', $this->('Another string')));`
* The module uses no caching, this might result in really long loading times for some parts
* Full export for all files (on the overview grid) takes really long, it is not recommended to use it on systems with a lot of modules installed

### License
[OSL-3.0](http://opensource.org/licenses/OSL-3.0)  
[AFL-3.0](http://opensource.org/licenses/AFL-3.0)