<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Adminhtml_TranslationhelperController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this->_title($this->__('Translation Helper'));
        $this->loadLayout();
        $this->_setActiveMenu('translationhelper');
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportAction()
    {
        // This might take a while ...
        ini_set('max_execution_time', 0);

        $zipFile    = tempnam(Mage::getBaseDir('var') . DS . 'export', 'zip');
        $zipArchive = new ZipArchive();
        $zipArchive->open($zipFile, ZipArchive::CREATE);

        $noMeta = $this->getRequest()->getParam('nometa');

        $csvFilesToDelete = array();

        $modules = (array)Mage::getConfig()->getNode('modules')->children();
        foreach ($modules as $moduleName => $module) {
            $this->getRequest()->setParam('module', Mage::helper('core')->urlEncode($moduleName));

            // Get grid block
            $gridBlock = $this->getLayout()->createBlock('poebel_translationhelper/adminhtml_detail_grid');
            // Initialize grid block with default columns and data
            $gridBlock->getHtml();

            if ($noMeta) {
                $gridBlock->removeColumn('area');
                $gridBlock->removeColumn('usage_area');
                $gridBlock->removeColumn('source_files_list');
            }

            foreach (Mage::helper('poebel_translationhelper')->getLocales() as $onlyLocale) {
                foreach (Mage::helper('poebel_translationhelper')->getLocales() as $locale) {
                    if ($locale != $onlyLocale) {
                        $gridBlock->removeColumn('translated_string_' . strtolower($locale));
                    } else {
                        $gridBlock->addColumnAfter(
                            'translated_string_' . strtolower($onlyLocale), array(
                            'header'   => $onlyLocale,
                            'index'    => 'translated_string_' . strtolower($onlyLocale),
                            'type'     => 'text',
                            'escape'   => true,
                            'truncate' => 9999,
                            'renderer' => 'poebel_translationhelper/adminhtml_widget_grid_column_renderer_translatedString',
                        ),
                            'source'
                        );
                    }
                }

                $csv = $gridBlock->getCsvFile(!$noMeta);

                if (is_array($csv) && isset($csv['value'])) {
                    $zipArchive->addEmptyDir(($onlyLocale ? $onlyLocale : 'all'));
                    $zipArchive->addFile(
                        $csv['value'],
                        ($onlyLocale ? $onlyLocale : 'all') . DS . Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')) . ($noMeta ? '' : '_with_meta') . '.csv'
                    );

                    // Delete CSVs after $zipArchive->close()
                    if (isset($csv['rm']) && $csv['rm']) {
                        $csvFilesToDelete[] = $csv['value'];
                    }
                }
            }
        }

        $zipArchive->close();

        // Delete CSVs after $zipArchive->close()
        foreach ($csvFilesToDelete as $csvFileToDelete) {
            unlink($csvFileToDelete);
        }

        $this->_prepareDownloadResponse(
            Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')) . ($noMeta ? '' : '_with_meta') . '.zip',
            array(
                'type'  => 'filename',
                'value' => $zipFile,
                'rm'    => true,
            )
        );
    }

    public function detailAction()
    {
        if (!$this->getRequest()->getParam('module')) {
            $this->_redirect('*/*/index');

            return;
        }

        $this->_title($this->__('Translations for %s', Mage::helper('core')->urlDecode($this->getRequest()->getParam('module'))));
        $this->loadLayout();
        $this->_setActiveMenu('translationhelper');
        $this->renderLayout();
    }

    public function detailGridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function exportDetailAction()
    {
        if (!$this->getRequest()->getParam('module')) {
            $this->_redirect('*/*/index');

            return;
        }

        if ($zip = $this->getRequest()->getParam('zip')) {
            $zipFile = tempnam(Mage::getBaseDir('var') . DS . 'export', 'zip');
            $zipArchive = new ZipArchive();
            $zipArchive->open($zipFile, ZipArchive::CREATE);

            $noMeta = $this->getRequest()->getParam('nometa');

            $csvFilesToDelete = array();

            // Get grid block
            $gridBlock = $this->getLayout()->createBlock('poebel_translationhelper/adminhtml_detail_grid');
            // Initialize grid block with default columns and data
            $gridBlock->getHtml();

            if ($noMeta) {
                $gridBlock->removeColumn('area');
                $gridBlock->removeColumn('usage_area');
                $gridBlock->removeColumn('source_files_list');
            }

            foreach (Mage::helper('poebel_translationhelper')->getLocales() as $onlyLocale) {
                foreach (Mage::helper('poebel_translationhelper')->getLocales() as $locale) {
                    if ($locale != $onlyLocale) {
                        $gridBlock->removeColumn('translated_string_' . strtolower($locale));
                    } else {
                        $gridBlock->addColumnAfter(
                            'translated_string_' . strtolower($onlyLocale), array(
                            'header'   => $onlyLocale,
                            'index'    => 'translated_string_' . strtolower($onlyLocale),
                            'type'     => 'text',
                            'escape'   => true,
                            'truncate' => 9999,
                            'renderer' => 'poebel_translationhelper/adminhtml_widget_grid_column_renderer_translatedString',
                        ),
                            'source'
                        );
                    }
                }

                $csv = $gridBlock->getCsvFile(!$noMeta);

                if (is_array($csv) && isset($csv['value'])) {
                    $zipArchive->addEmptyDir($onlyLocale);
                    $zipArchive->addFile(
                        $csv['value'],
                        $onlyLocale . DS . Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')) . ($noMeta ? '' : '_with_context') . '.csv'
                    );

                    // Delete CSVs after $zipArchive->close()
                    if (isset($csv['rm']) && $csv['rm']) {
                        $csvFilesToDelete[] = $csv['value'];
                    }
                }
            }

            $zipArchive->close();

            // Delete CSVs after $zipArchive->close()
            foreach ($csvFilesToDelete as $csvFileToDelete) {
                unlink($csvFileToDelete);
            }

            $this->_prepareDownloadResponse(
                Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')) . ($noMeta ? '' : '_with_context') . '.zip',
                array(
                    'type'  => 'filename',
                    'value' => $zipFile,
                    'rm'    => true,
                )
            );
        } else {
            // Get grid block
            $gridBlock = $this->getLayout()->createBlock('poebel_translationhelper/adminhtml_detail_grid');
            // Initialize grid block with default columns and data
            $gridBlock->getHtml();

            if ($onlyLocale = $this->getRequest()->getParam('locale')) {
                foreach (Mage::helper('poebel_translationhelper')->getLocales() as $locale) {
                    if ($locale != $onlyLocale) {
                        $gridBlock->removeColumn('translated_string_' . strtolower($locale));
                    }
                }
            }

            if ($noMeta = $this->getRequest()->getParam('nometa')) {
                $gridBlock->removeColumn('area');
                $gridBlock->removeColumn('usage_area');
                $gridBlock->removeColumn('source_files');
            }

            $this->_prepareDownloadResponse(
                Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')) . ($onlyLocale ? '_' . $onlyLocale : '_all') . ($noMeta ? '' : '_with_context') . '.csv',
                $gridBlock->getCsvFile()
            );
        }
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('poebel_translationhelper');
    }
}
