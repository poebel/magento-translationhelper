<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Helper_Abstract extends Mage_Core_Helper_Abstract
{
    const XML_PATH_GENERAL_LOCALES              = 'poebel_translationhelper/general/locales';
    const XML_PATH_GENERAL_ONLY_FROM_SAME_SCOPE = 'poebel_translationhelper/general/only_from_same_scope';

    public function getLocales()
    {
        return explode(',', Mage::getStoreConfig(self::XML_PATH_GENERAL_LOCALES));
    }

    public function getOnlyFromSameScope()
    {
        return (bool)Mage::getStoreConfig(self::XML_PATH_GENERAL_ONLY_FROM_SAME_SCOPE);
    }
}