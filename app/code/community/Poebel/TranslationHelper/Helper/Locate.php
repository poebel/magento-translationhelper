<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Helper_Locate extends Poebel_TranslationHelper_Helper_Abstract
{
    protected function _moduleNameToDir($moduleName)
    {
        return str_replace(' ', DS, ucwords(str_replace('_', ' ', trim($moduleName))));
    }

    protected function _getPossibleCodeLocations($moduleName)
    {
        $moduleDir = $this->_moduleNameToDir($moduleName);

        return array(
            Mage::getBaseDir('lib') . DS . $moduleDir,
            Mage::getBaseDir('code') . DS . 'local' . DS . $moduleDir,
            Mage::getBaseDir('code') . DS . 'community' . DS . $moduleDir,
            Mage::getBaseDir('code') . DS . 'core' . DS . $moduleDir,
        );
    }

    protected function _getPossibleXmlLocations($moduleName)
    {
        $moduleDir = $this->_moduleNameToDir($moduleName);

        return array(
            Mage::getBaseDir('code') . DS . 'local' . DS . $moduleDir,
            Mage::getBaseDir('code') . DS . 'community' . DS . $moduleDir,
            Mage::getBaseDir('code') . DS . 'core' . DS . $moduleDir,
        );
    }

    protected function _findFilesByPatternRecursively($pattern, $dir)
    {
        $files = glob($dir . DS . $pattern);
        foreach (glob($dir . DS . '*', GLOB_ONLYDIR) as $subDir) {
            $files = array_merge($files, $this->_findFilesByPatternRecursively($pattern, $subDir));
        }
        $files = array_unique($files);
        sort($files);
        return $files;
    }

    protected function _getDesignFilesFromXmlFiles($xmlFileArray)
    {
        $fileArray = array(
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML             => array(),
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_TEMPLATE        => array(),
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_STATIC_TEMPLATE => array(),
        );

        $enclosures = array(
            array(
                'start' => '[',
                'end'   => ']',
            ),
            array(
                'start' => '>',
                'end'   => '<',
            ),
            array(
                'start' => 'template="',
                'end'   => '"',
            ),
        );

        $designDir = Mage::getBaseDir('design');
        $localeDir = Mage::getBaseDir('locale');

        foreach ($xmlFileArray as $xmlFile) {
            $content = file_get_contents($xmlFile);
            foreach ($fileArray as $fileType => &$fileTypeArray) {
                $possibleFiles = array();

                foreach ($enclosures as $enclosure) {
                    $invalidCharacters = preg_quote('\\><"|*?', '/');
                    $regex             = '/' . preg_quote($enclosure['start'], '/') . '([^' . $invalidCharacters . ']*\.' . $fileType . ')' . preg_quote($enclosure['end'], '/') . '/i';
                    preg_match_all($regex, $content, $_possibleFiles);
                    if (isset($_possibleFiles[1]) && is_array($_possibleFiles[1])) {
                        $possibleFiles = array_merge($possibleFiles, $_possibleFiles[1]);
                    }
                }

                foreach ($possibleFiles as $possibleFile) {
                    // Admin layout and templates
                    foreach (glob($designDir . DS . 'adminhtml' . DS . '*', GLOB_ONLYDIR) as $package) {
                        foreach (glob($package . DS . '*', GLOB_ONLYDIR) as $theme) {
                            $fileTypeArray = array_merge($fileTypeArray, $this->_findFilesByPatternRecursively($possibleFile, $theme . DS . 'layout'));
                        }
                    }
                    foreach (glob($designDir . DS . 'adminhtml' . DS . '*', GLOB_ONLYDIR) as $package) {
                        foreach (glob($package . DS . '*', GLOB_ONLYDIR) as $theme) {
                            $fileTypeArray = array_merge($fileTypeArray, $this->_findFilesByPatternRecursively($possibleFile, $theme . DS . 'template'));
                        }
                    }
                    // Frontend layout and templates
                    foreach (glob($designDir . DS . 'frontend' . DS . '*', GLOB_ONLYDIR) as $package) {
                        foreach (glob($package . DS . '*', GLOB_ONLYDIR) as $theme) {
                            $fileTypeArray = array_merge($fileTypeArray, $this->_findFilesByPatternRecursively($possibleFile, $theme . DS . 'layout'));
                        }
                    }
                    foreach (glob($designDir . DS . 'frontend' . DS . '*', GLOB_ONLYDIR) as $package) {
                        foreach (glob($package . DS . '*', GLOB_ONLYDIR) as $theme) {
                            $fileTypeArray = array_merge($fileTypeArray, $this->_findFilesByPatternRecursively($possibleFile, $theme . DS . 'template'));
                        }
                    }
                    // Email templates
                    foreach (glob($localeDir . DS . '*', GLOB_ONLYDIR) as $locale) {
                        $fileTypeArray = array_merge($fileTypeArray, $this->_findFilesByPatternRecursively($possibleFile, $locale . DS . 'template' . DS . 'email'));
                    }
                }
            }
        }

        return $fileArray;
    }

    protected function _getModuleFiles($moduleName)
    {
        $fileArray = array(
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_CODE            => array(),
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML             => array(),
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_TEMPLATE        => array(),
            Poebel_TranslationHelper_Model_Detail::FILE_TYPE_STATIC_TEMPLATE => array(),
        );

        // Code
        foreach ($this->_getPossibleCodeLocations($moduleName) as $possibleCodeLocation) {
            $fileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_CODE] = array_merge(
                $fileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_CODE],
                $this->_findFilesByPatternRecursively('*.' . Poebel_TranslationHelper_Model_Detail::FILE_TYPE_CODE, $possibleCodeLocation)
            );
        }

        // Config XMLs
        foreach ($this->_getPossibleXmlLocations($moduleName) as $possibleXmlLocation) {
            $fileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML] = array_merge(
                $fileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML],
                $this->_findFilesByPatternRecursively('*.' . Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML, $possibleXmlLocation)
            );
        }

        // Design files configured in config XMLs
        $designFileArray = $this->_getDesignFilesFromXmlFiles($fileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML]);

        // Design files configured in design XMLs
        $additionalDesignFileArray = $this->_getDesignFilesFromXmlFiles($designFileArray[Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML]);

        // Merge all files together
        $fileArray = array_merge_recursive($fileArray, $designFileArray, $additionalDesignFileArray);

        // Get real file path and remove duplicates
        foreach ($fileArray as &$fileTypeArray) {
            foreach ($fileTypeArray as &$file) {
                $file = realpath($file);
            }
            $fileTypeArray = array_unique($fileTypeArray);
            sort($fileTypeArray);
        }

        return $fileArray;
    }

    protected function _getTranslationsFromCodeFile($file)
    {
        $content = file_get_contents($file);

        $translations = array();

        /**
         * ToDo: current method does not work very reliable:
         * - fails if termination character is inside translated string
         * - fails on multi line translations
         */
        preg_match_all('/__\(\'(.*)\'[\,\)]{1}/U', $content, $_translations);
        if (isset($_translations[1]) && is_array($_translations[1])) {
            $translations = array_merge($translations, $_translations[1]);
        }

        preg_match_all('/__\(\"(.*)\"\)/U', $content, $_translations);
        if (isset($_translations[1]) && is_array($_translations[1])) {
            $translations = array_merge($translations, $_translations[1]);
        }

        $translations = array_unique($translations);
        sort($translations);

        return $translations;
    }

    protected function _getTranslationsFromXmlFile($file)
    {
        $content = file_get_contents($file);

        $translations     = array();
        $translatableTags = array();

        preg_match_all('/translate\=\"(.*)"/U', $content, $_translatableTags);
        if (isset($_translatableTags[1]) && is_array($_translatableTags[1])) {
            foreach ($_translatableTags[1] as $_translatableTag) {
                $translatableTags = array_merge($translatableTags, explode(' ', $_translatableTag));
            }
        }

        $translatableTags = array_unique($translatableTags);

        foreach ($translatableTags as $translatableTag) {
            preg_match_all('/\<' . preg_quote($translatableTag) . '\>(.*)\<\/' . preg_quote($translatableTag) . '\>/U', $content, $_translations);
            if (isset($_translations[1]) && is_array($_translations[1])) {
                $translations = array_merge($translations, $_translations[1]);
            }
        }

        $translations = array_unique($translations);
        sort($translations);

        foreach ($translations as &$translation) {
            $translation = str_replace('<![CDATA[', '', $translation);
            $translation = str_replace(']]>', '', $translation);
        }

        return $translations;
    }

    protected function _addCoreTranslation($moduleName, $translationObjects)
    {
        $coreTranslate = Mage::getModel('poebel_translationhelper/extend_core_translate')
                             ->setOnlyFromScope($this->getOnlyFromSameScope())
                             ->setTranslateInline(false);

        foreach ($this->getLocales() as $locale) {
            // Change locale globally to prevent wrong theme translations from being loaded
            $_currentLocaleCode = Mage::app()->getLocale()->getLocaleCode();
            Mage::app()->getLocale()->setLocaleCode($locale);

            $coreTranslate
                ->setLocale($locale)
                ->init('adminhtml', true);

            foreach ($translationObjects as $translationObject) {

                $translateExpression = Mage::getModel('core/translate_expr')
                                           ->setText($translationObject->getSource())
                                           ->setModule($moduleName);

                $translation = $coreTranslate->translate(array($translateExpression));

                if ($translation != $translationObject->getSource()) {
                    $translationObject->addTranslatedString($translation, $locale);
                }
            }

            Mage::app()->getLocale()->setLocaleCode($_currentLocaleCode);
        }

        return $this;
    }

    protected function _addCsvTranslation($moduleName, &$translationObjects)
    {
        // Fetch valid scopes
        $validScopes = array();
        foreach (glob(Mage::getBaseDir('locale'), GLOB_ONLYDIR) as $localeDir) {
            foreach (glob($localeDir . DS . '*.csv') as $localeFile) {
                $validScopes[] = basename($localeFile, '.csv');
            }
        }
        $validScopes = array_unique($validScopes);

        // Add translations from CSV files
        foreach ($this->getLocales() as $locale) {
            $localeFile = Mage::getBaseDir('locale') . DS . $locale . DS . $moduleName . '.csv';

            if (file_exists($localeFile)) {
                $io          = new Varien_File_Csv();
                $_localeData = $io->getDataPairs($localeFile);

                foreach ($_localeData as $_source => $_translation) {
                    $_source = explode(Mage_Core_Model_Translate::SCOPE_SEPARATOR, $_source);
                    if (in_array($_source[0], $validScopes)) {
                        if ($this->getOnlyFromSameScope()) {
                            if ($_source[0] == $moduleName) {
                                array_shift($_source);
                            } else {
                                continue;
                            }
                        } else {
                            array_shift($_source);
                        }
                    }
                    $_source = implode(Mage_Core_Model_Translate::SCOPE_SEPARATOR, $_source);

                    if (!isset($translationObjects[$_source])) {
                        $translationObjects[$_source] = Mage::getModel('poebel_translationhelper/detail');
                        $translationObjects[$_source]->setSource($_source);
                        $translationObjects[$_source]->addSourceFile($localeFile);
                        $translationObjects[$_source]->setAreaBySourceFiles();
                    }

                    $translationObjects[$_source]->setInCsv(true);
                }
            }
        }
    }

    public function getModuleTranslationObjects($moduleName)
    {
        $translationObjects = array();

        foreach ($this->_getModuleFiles($moduleName) as $fileType => $fileTypeArray) {
            foreach ($fileTypeArray as $file) {
                if ($fileType == Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML) {
                    $translations = $this->_getTranslationsFromXmlFile($file);
                } else {
                    $translations = $this->_getTranslationsFromCodeFile($file);
                }

                foreach ($translations as $translation) {
                    if (!isset($translationObjects[$translation])) {
                        $translationObject = Mage::getModel('poebel_translationhelper/detail');
                        $translationObject->setSource($translation);
                    } else {
                        $translationObject = $translationObjects[$translation];
                    }

                    $translationObject->addSourceFile($file);
                    $translationObject->setAreaBySourceFiles();

                    switch ($fileType) {
                    case Poebel_TranslationHelper_Model_Detail::FILE_TYPE_CODE:
                        $translationObject->setInCode(true);
                        break;
                    case Poebel_TranslationHelper_Model_Detail::FILE_TYPE_XML:
                        $translationObject->setInXml(true);
                        break;
                    case Poebel_TranslationHelper_Model_Detail::FILE_TYPE_TEMPLATE:
                        $translationObject->setInTemplate(true);
                        break;
                    case Poebel_TranslationHelper_Model_Detail::FILE_TYPE_STATIC_TEMPLATE:
                        $translationObject->setInTemplate(true);
                        break;
                    }

                    $translationObjects[$translation] = $translationObject;
                }
            }
        }

        $this->_addCsvTranslation($moduleName, $translationObjects);

        $this->_addCoreTranslation($moduleName, $translationObjects);

        ksort($translationObjects);

        return $translationObjects;
    }
}