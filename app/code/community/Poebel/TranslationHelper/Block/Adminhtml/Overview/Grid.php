<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Block_Adminhtml_Overview_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('poebel_translationhelper_overview_grid');
        $this->setDefaultSort('module_name');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('poebel_translationhelper/overview_collection');
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('poebel_translationhelper');

        $this->addColumn(
            'module_name', array(
                'header' => $helper->__('Module Name'),
                'index'  => 'module_name',
                'type'   => 'text',
            )
        );

        $this->addColumn(
            'is_active', array(
                'header'  => $helper->__('Is Active'),
                'index'   => 'is_active',
                'type'    => 'options',
                'options' => Mage::getSingleton('adminhtml/system_config_source_yesno')->toArray(),
            )
        );

        $this->addColumn(
            'code_pool', array(
                'header'  => $helper->__('Code Pool'),
                'index'   => 'code_pool',
                'type'    => 'options',
                'options' => Mage::getSingleton('poebel_translationhelper/system_config_source_codePool')->toArray(),
            )
        );

        $this->addColumn(
            'version', array(
                'header' => $helper->__('Version'),
                'index'  => 'version',
                'type'   => 'text',
            )
        );

        $this->addExportType('*/*/export/nometa/1', $helper->__('ZIP All Locales'));
        $this->addExportType('*/*/export', $helper->__('ZIP All Locales (with context)'));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current' => true));
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/detail', array('module' => Mage::helper('core')->urlEncode($row->getModuleName())));
    }
}