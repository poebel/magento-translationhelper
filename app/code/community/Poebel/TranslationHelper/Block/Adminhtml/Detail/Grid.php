<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Block_Adminhtml_Detail_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    protected $_exportCollection            = null;
    protected $_columnsInitializedForExport = false;

    public function __construct()
    {
        parent::__construct();
        $this->setId('poebel_translationhelper_detail_grid_' . Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')));
        $this->setDefaultSort('source');
        $this->setDefaultDir('ASC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
        // Load dummy collection for export purposes
        if ($this->_isExport) {
            $collection = Mage::getResourceModel('poebel_translationhelper/collection_dummy');
        } else {
            $collection = Mage::getResourceModel('poebel_translationhelper/detail_collection');
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // Do not reinitialize columns on export
        if ($this->_isExport && $this->_columnsInitializedForExport) {
            return parent::_prepareColumns();
        }
        $this->_columnsInitializedForExport = true;

        $helper = Mage::helper('poebel_translationhelper');

        $this->addColumn(
            'area', array(
                'header'  => $helper->__('Area'),
                'index'   => 'area',
                'type'    => 'options',
                'getter'  => 'getArea',
                'options' => Mage::getSingleton('poebel_translationhelper/system_config_source_area')->toArray(),
            )
        );

        $this->addColumn(
            'usage_area', array(
                'header' => $helper->__('Usage Area'),
                'index'  => 'usage_area',
                'type'   => 'text',
                'getter' => 'getUsageArea',
            )
        );

        $this->addColumn(
            'source_files_list', array(
                'header'   => $helper->__('File(s)'),
                'index'    => 'source_files_list',
                'type'     => 'text',
                'getter'   => 'getSourceFilesList',
                'truncate' => 9999,
            )
        );

        $this->addColumn(
            'source', array(
                'header'   => $helper->__('Source'),
                'index'    => 'source',
                'type'     => 'text',
                'escape'   => true,
                'truncate' => 9999,
            )
        );

        foreach (Mage::helper('poebel_translationhelper')->getLocales() as $locale) {
            $this->addColumn(
                'translated_string_' . strtolower($locale), array(
                    'header'   => $locale,
                    'index'    => 'translated_string_' . strtolower($locale),
                    'type'     => 'text',
                    'escape'   => true,
                    'truncate' => 9999,
                    'renderer' => 'poebel_translationhelper/adminhtml_widget_grid_column_renderer_translatedString',
                )
            );
        }

        $this->addExportType('*/*/exportDetail/zip/1/nometa/1/module/' . $this->getRequest()->getParam('module'), $helper->__('ZIP All Locales'));
        $this->addExportType('*/*/exportDetail/zip/1/module/' . $this->getRequest()->getParam('module'), $helper->__('ZIP All Locales (with context)'));
        $this->addExportType('*/*/exportDetail/nometa/1/module/' . $this->getRequest()->getParam('module'), $helper->__('CSV All Locales'));
        $this->addExportType('*/*/exportDetail/module/' . $this->getRequest()->getParam('module'), $helper->__('CSV All Locales (with context)'));
        foreach (Mage::helper('poebel_translationhelper')->getLocales() as $locale) {
            $this->addExportType('*/*/exportDetail/locale/' . $locale . '/nometa/1/module/' . $this->getRequest()->getParam('module'), $helper->__('CSV %s', $locale));
            $this->addExportType('*/*/exportDetail/locale/' . $locale . '/module/' . $this->getRequest()->getParam('module'), $helper->__('CSV %s (with context)', $locale));
        }

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/detailGrid', array('_current' => true));
    }

    /**
     * Retrieve a file container array by grid data as CSV
     *
     * Return array with keys type and value
     *
     * @return array
     */
    public function getCsvFile($withHeader = true)
    {
        $this->_isExport = true;
        $this->_prepareGrid();

        $io = new Varien_Io_File();

        $path = Mage::getBaseDir('var') . DS . 'export' . DS;
        $name = md5(microtime());
        $file = $path . DS . $name . '.csv';

        $io->setAllowCreateFolders(true);
        $io->open(array('path' => $path));
        $io->streamOpen($file, 'w+');
        $io->streamLock(true);
        if ($withHeader) {
            $io->streamWriteCsv($this->_getExportHeaders());
        }

        $this->_exportIterateCollection('_exportCsvItem', array($io));

        if ($this->getCountTotals()) {
            $io->streamWriteCsv($this->_getExportTotals());
        }

        $io->streamUnlock();
        $io->streamClose();

        return array(
            'type'  => 'filename',
            'value' => $file,
            'rm'    => true // can delete file after use
        );
    }

    /**
     * Iterate collection and call callback method per item
     * For callback method first argument always is item object
     *
     * @param string $callback
     * @param array  $args additional arguments for callback method
     *
     * @return Mage_Adminhtml_Block_Widget_Grid
     */
    public function _exportIterateCollection($callback, array $args)
    {
        if (is_null($this->_exportCollection)) {
            $collection = Mage::getResourceModel('poebel_translationhelper/detail_collection');
            $collection->setPageSize(999999999999999);
            $collection->setCurPage(1);
            $collection->load();
            $this->_exportCollection = $collection;
        } else {
            $collection = $this->_exportCollection;
        }

        foreach ($collection as $item) {
            call_user_func_array(array($this, $callback), array_merge(array($item), $args));
        }
    }
}