<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Block_Adminhtml_Detail extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'poebel_translationhelper';
        $this->_controller = 'adminhtml_detail';
        $this->_headerText = Mage::helper('poebel_translationhelper')->__('Translations for %s', Mage::helper('core')->urlDecode($this->getRequest()->getParam('module')));

        parent::__construct();

        $this->_removeButton('add');
        $this->_addBackButton();
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*');
    }
}