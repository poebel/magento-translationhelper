<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_Resource_Detail_Collection extends Poebel_TranslationHelper_Model_Resource_Collection_Abstract
{
    public function loadData($printQuery = false, $logQuery = false)
    {
        if (!$this->isLoaded()) {
            $helper     = Mage::helper('poebel_translationhelper/locate');
            $moduleName = $helper->urlDecode(Mage::app()->getRequest()->getParam('module'));

            $this->_addItemsWithFilterAndSorting($helper->getModuleTranslationObjects($moduleName));

            $this->_setIsLoaded(true);
        }

        return $this;
    }
}