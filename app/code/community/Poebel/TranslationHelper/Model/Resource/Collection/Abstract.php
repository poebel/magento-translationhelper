<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_Resource_Collection_Abstract extends Varien_Data_Collection
{
    public function addFieldToFilter($field, $filter)
    {
        $this->_filters[$field] = $filter;

        return $this;
    }

    protected function _addItemsWithFilterAndSorting($items)
    {
        $_items         = array();
        $_sortDirection = null;

        if (count($this->_orders) > 1) {
            throw new Mage_Core_Exception('Multi column sorting is not supported');
        }

        foreach ($items as $item) {
            if ($this->_isItemAllowed($item)) {
                if (count($this->_orders) === 1) {
                    foreach ($this->_orders as $field => $direction) {
                        $_hash = array();
                        foreach ($item->getData() as $_data) {
                            if (is_array($_data)) {
                                $_data = implode('_', $_data);
                            }
                            $_hash[] = $_data;
                        }
                        $_hash                                                    = md5(implode('_', $_hash));
                        $_sortDirection                                           = $direction;
                        $_items[$item->getDataUsingMethod($field) . '_' . $_hash] = $item;
                    }
                } else {
                    $_items[] = $item;
                }
            }
        }

        if ($_sortDirection) {
            if (strtoupper($_sortDirection) === 'DESC') {
                krsort($_items);
            } else {
                ksort($_items);
            }
        }

        $this->_totalRecords = count($_items);

        $curPage  = ($this->_curPage ? $this->_curPage : 1);
        $pageSize = ($this->_pageSize ? $this->_pageSize : 20);

        $startIdx = (($curPage * $pageSize) - $pageSize + 1);
        $lastIdx  = ($startIdx + $pageSize - 1);

        $curIdx = 1;

        foreach ($_items as $_item) {
            if ($curIdx < $startIdx) {
                $curIdx++;
                continue;
            }

            if ($curIdx > $lastIdx) {
                break;
            }

            $this->addItem($_item);
            $curIdx++;
        }

        return $this;
    }

    protected function _isItemAllowed(Varien_Object $item)
    {
        foreach ($this->_filters as $field => $filter) {
            if (!$this->_isValueAllowed($item->getDataUsingMethod($field), $filter)) {
                return false;
            }
        }

        return true;
    }

    protected function _isValueAllowed($value, $filter)
    {
        if (is_array($filter)) {
            foreach ($filter as $condition => $filterValue) {
                switch ($condition) {
                case 'eq':
                    if ($value !== $filterValue) {
                        return false;
                    }
                    break;
                case 'like':
                    if ($filterValue instanceof Zend_Db_Expr) {
                        $filterValue = rtrim(ltrim((string)$filterValue, "'"), "'");
                    }
                    $_filterValues = explode('%', $filterValue);
                    foreach ($_filterValues as $_filterValue) {
                        $_filterValue = trim($_filterValue);
                        if ($_filterValue && stripos($value, $_filterValue) === false) {
                            return false;
                        }
                    }
                    break;
                default:
                    throw new Mage_Core_Exception('Unsupported filter condition: ' . $condition);
                    break;
                }
            }

            return true;
        }

        return (bool)($value === $filter);
    }
}