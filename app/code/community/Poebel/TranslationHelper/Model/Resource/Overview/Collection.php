<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_Resource_Overview_Collection extends Poebel_TranslationHelper_Model_Resource_Collection_Abstract
{
    public function loadData($printQuery = false, $logQuery = false)
    {
        if (!$this->isLoaded()) {
            $items = array();

            $modules = (array)Mage::getConfig()->getNode('modules')->children();
            foreach ($modules as $moduleName => $module) {
                $items[] = Mage::getModel('poebel_translationhelper/overview')
                    ->setData(
                        array(
                            'module_name' => $moduleName,
                            'is_active'   => ($module->is('active') ? 1 : 0),
                            'code_pool'   => (string)$module->codePool,
                            'version'     => (string)$module->version,
                        )
                    );
            }

            $this->_addItemsWithFilterAndSorting($items);

            $this->_setIsLoaded(true);
        }

        return $this;
    }
}