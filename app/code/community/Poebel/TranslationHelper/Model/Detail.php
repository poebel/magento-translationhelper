<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_Detail extends Varien_Object
{
    const AREA_FRONTEND = 'frontend';
    const AREA_BACKEND  = 'backend';
    const AREA_BOTH     = 'both';
    const AREA_UNKNOWN  = 'unknown';

    const FILE_TYPE_CODE            = 'php';
    const FILE_TYPE_XML             = 'xml';
    const FILE_TYPE_TEMPLATE        = 'phtml';
    const FILE_TYPE_STATIC_TEMPLATE = 'html';
    const FILE_TYPE_CSV             = 'csv';

    protected $_backendFilePathParts
        = array(
            '/adminhtml/',
            '/Adminhtml/',
            '/admin/',
            '/Admin/',
            '/etc/adminhtml.xml',
            '/etc/config.xml',
            '/etc/system.xml',
        );

    protected $_frontendFilePathParts
        = array(
            '/frontend/',
        );

    /**
     * @return array
     */
    public function getSourceFiles()
    {
        if (!is_array($this->getData('source_files'))) {
            return array();
        }

        return $this->getData('source_files');
    }

    /**
     * @return string
     */
    public function getSourceFilesList($glue = '<br/>')
    {
        return implode($glue, $this->getSourceFiles());
    }

    /**
     * @param string $sourceFile
     *
     * @return $this
     */
    public function addSourceFile($sourceFile)
    {
        $sourceFile = str_replace(Mage::getBaseDir() . DS, '', $sourceFile);

        if (strpos($sourceFile, '.modman') === 0) {
            $sourceFile = explode(DS, $sourceFile);
            array_shift($sourceFile);
            array_shift($sourceFile);
            $sourceFile = implode(DS, $sourceFile);
        }

        $sourceFiles = $this->getSourceFiles();

        if (!in_array($sourceFile, $sourceFiles)) {
            $sourceFiles[] = $sourceFile;
            sort($sourceFiles);
        }

        return $this->setSourceFiles($sourceFiles);
    }

    /**
     * @return string
     */
    public function getUsageArea()
    {
        $usageArea = array();

        if ($this->getInCsv()) {
            $usageArea[] = 'CSV';
        }
        if ($this->getInCode()) {
            $usageArea[] = 'Code';
        }
        if ($this->getInTemplate()) {
            $usageArea[] = 'Template';
        }
        if ($this->getInXml()) {
            $usageArea[] = 'XML';
        }

        return implode(', ', $usageArea);
    }

    /**
     * @return null|string
     */
    public function getArea()
    {
        if (is_null($this->getData('area'))) {
            $this->setAreaBySourceFiles();
        }

        return $this->getData('area');
    }

    /**
     * @return $this
     */
    public function setAreaBySourceFiles()
    {
        $area = self::AREA_UNKNOWN;

        foreach ($this->getSourceFiles() as $sourceFile) {
            foreach ($this->_backendFilePathParts as $backendFilePathPart) {
                $backendFilePathPart = str_replace('/', DS, $backendFilePathPart);
                if (strpos($sourceFile, $backendFilePathPart) !== false) {
                    if ($area == self::AREA_FRONTEND) {
                        $this->setArea(self::AREA_BOTH);

                        return $this;
                    }

                    $area = self::AREA_BACKEND;
                }
            }

            foreach ($this->_frontendFilePathParts as $frontendFilePathPart) {
                $frontendFilePathPart = str_replace('/', DS, $frontendFilePathPart);
                if (strpos($sourceFile, $frontendFilePathPart) !== false) {
                    if ($area == self::AREA_BACKEND) {
                        $this->setArea(self::AREA_BOTH);

                        return $this;
                    }

                    $area = self::AREA_FRONTEND;
                }
            }
        }

        return $this->setArea($area);
    }

    /**
     * @param string $translatedString
     * @param string $locale
     *
     * @return $this
     */
    public function addTranslatedString($translatedString, $locale)
    {
        return $this->setData('translated_string_' . strtolower($locale), $translatedString);
    }

    /**
     * @param array $translatedStrings
     *
     * @return $this
     */
    public function addTranslatedStrings($translatedStrings)
    {
        foreach ($translatedStrings as $locale => $translatedString) {
            $this->addTranslatedString($translatedString, $locale);
        }

        return $this;
    }
}