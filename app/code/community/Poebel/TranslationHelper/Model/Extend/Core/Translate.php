<?php

class Poebel_TranslationHelper_Model_Extend_Core_Translate extends Mage_Core_Model_Translate
{
    protected $_onlyFromScope = false;

    /**
     * @param boolean $onlyFromScope
     *
     * @return $this
     */
    public function setOnlyFromScope($onlyFromScope)
    {
        $this->_onlyFromScope = (bool)$onlyFromScope;

        return $this;
    }

    /**
     * @return bool
     */
    public function getOnlyFromScope()
    {
        return $this->_onlyFromScope;
    }

    /**
     * Adding translation data
     *
     * @param array  $data
     * @param string $scope
     *
     * @return Mage_Core_Model_Translate
     */
    protected function _addData($data, $scope, $forceReload = false)
    {
        foreach ($data as $key => $value) {
            if ($key === $value) {
                continue;
            }
            $key   = $this->_prepareDataString($key);
            $value = $this->_prepareDataString($value);
            if ($scope && isset($this->_dataScope[$key]) && !$forceReload) {
                /**
                 * Checking previos value
                 */
                $scopeKey = $this->_dataScope[$key] . self::SCOPE_SEPARATOR . $key;
                if (!isset($this->_data[$scopeKey])) {
                    if (isset($this->_data[$key])) {
                        $this->_data[$scopeKey] = $this->_data[$key];
                        /**
                         * Not allow use translation not related to module
                         */
                        if ($this->getOnlyFromScope() && Mage::getIsDeveloperMode()) {
                            unset($this->_data[$key]);
                        }
                    }
                }
                $scopeKey               = $scope . self::SCOPE_SEPARATOR . $key;
                $this->_data[$scopeKey] = $value;
            } elseif ($scope && $this->getOnlyFromScope()) {
                unset($this->_data[$key]);
                $scopeKey               = $scope . self::SCOPE_SEPARATOR . $key;
                $this->_data[$scopeKey] = $value;
                $this->_dataScope[$key] = $scope;
            } elseif(!$this->getOnlyFromScope()) {
                $this->_data[$key]      = $value;
                $this->_dataScope[$key] = $scope;
            }
        }
        return $this;
    }
}