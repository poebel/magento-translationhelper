<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
abstract class Poebel_TranslationHelper_Model_System_Config_Source_Abstract
{
    protected $_optionArray = null;
    protected $_array       = null;
    protected $_empty       = false;

    abstract public function toOptionArray();

    public function toArray()
    {
        if (is_null($this->_array)) {
            $this->_array = array();

            if ($this->_empty) {
                $this->_array[] = $this->_empty;
            }

            foreach ($this->toOptionArray() as $option) {
                if (isset($option['value']) && !empty($option['value']) && isset($option['label'])) {
                    $this->_array[$option['value']] = $option['label'];
                }
            }
        }

        return $this->_array;
    }
}