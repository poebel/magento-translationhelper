<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_System_Config_Source_CodePool extends Poebel_TranslationHelper_Model_System_Config_Source_Abstract
{
    const CODE_POOL_CORE      = 'core';
    const CODE_POOL_COMMUNITY = 'community';
    const CODE_POOL_LOCAL     = 'local';

    public function toOptionArray()
    {
        if (is_null($this->_optionArray)) {
            $this->_optionArray = array(
                array(
                    'value' => self::CODE_POOL_CORE,
                    'label' => 'Core',
                ),
                array(
                    'value' => self::CODE_POOL_COMMUNITY,
                    'label' => 'Community',
                ),
                array(
                    'value' => self::CODE_POOL_LOCAL,
                    'label' => 'Local',
                ),
            );
        }

        return $this->_optionArray;
    }
}