<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 */
class Poebel_TranslationHelper_Model_System_Config_Source_Area extends Poebel_TranslationHelper_Model_System_Config_Source_Abstract
{
    public function toOptionArray()
    {
        if (is_null($this->_optionArray)) {
            $this->_optionArray = array(
                array(
                    'value' => Poebel_TranslationHelper_Model_Detail::AREA_FRONTEND,
                    'label' => 'Frontend',
                ),
                array(
                    'value' => Poebel_TranslationHelper_Model_Detail::AREA_BACKEND,
                    'label' => 'Backend',
                ),
                array(
                    'value' => Poebel_TranslationHelper_Model_Detail::AREA_BOTH,
                    'label' => 'Both',
                ),
                array(
                    'value' => Poebel_TranslationHelper_Model_Detail::AREA_UNKNOWN,
                    'label' => 'Unknown',
                ),
            );
        }

        return $this->_optionArray;
    }
}